#include "mainwindow.h"
#include "ui_mainwindow.h"

#define NO_PAQUET "Aucuns paquets disponible ..."
//--------------------Constructeur-------------------------//

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   QObject::connect(ui->add_deck, SIGNAL(clicked()), this, SLOT(slot_ajouterpaquet()));
   QObject::connect(ui->add_card, SIGNAL(clicked()), this, SLOT(slot_ajoutercarte()));
   this->rafraichirAffichagePaquets();
}


MainWindow::~MainWindow()
{
    delete ui;
}


//--------------------SLOT-------------------------//

void MainWindow::slot_ajouterpaquet(){
    bool ok = false;
    QString nom_paquet = QInputDialog::getText(this, "Ajouter paquet", "Quel est le nom de votre nouveau paquet", QLineEdit::Normal, QString(), &ok);

    while(ok && nom_paquet.isEmpty()){
        QMessageBox::critical(this, "Ajouter paquet", "Vous n'avez pas voulu donner votre nom… snif.");
        nom_paquet = QInputDialog::getText(this, "Ajouter paquet", "Quel est le nom de votre nouveau paquet", QLineEdit::Normal, QString(), &ok);
    }

    if(ok){
        PaquetCartes paquet(nom_paquet.toUtf8().constData());
        gp.ajouterPaquet(paquet);
        this->rafraichirAffichagePaquets();
    }
}

void MainWindow::slot_ajoutercarte(){
    bool ok = false;

    QListWidgetItem* item = ui->listePaquets->currentItem();

    if(!item || !QString::compare(item->text(),QString::fromStdString(NO_PAQUET),Qt::CaseSensitive)) {
        QMessageBox::critical(this, "Ajouter carte", "Veuillez sélectionner un paquet");
        return;
    }

    QString nom_face = QInputDialog::getText(this, "Ajouter carte", "Quel est le nom de la face avant ? ", QLineEdit::Normal, QString(), &ok);

    while(ok && nom_face.isEmpty()){
        QMessageBox::critical(this, "Ajouter carte", "Vous n'avez pas voulu donner votre nom… snif.");
        nom_face = QInputDialog::getText(this, "Ajouter carte", "Quel est le nom de votre nouveau paquet", QLineEdit::Normal, QString(), &ok);
    }

    if(!ok)
        return;

    ok = false;

    QString nom_dos = QInputDialog::getText(this, "Ajouter carte", "Quel est le nom du dos de la carte ? ", QLineEdit::Normal, QString(), &ok);

    while(ok && nom_dos.isEmpty()){
        QMessageBox::critical(this, "Ajouter carte", "Vous n'avez pas voulu donner votre nom… snif.");
        nom_face = QInputDialog::getText(this, "Ajouter carte", "Quel est le nom de votre nouveau paquet", QLineEdit::Normal, QString(), &ok);
    }

    if(ok){
        Carte carte(nom_face.toUtf8().constData(), nom_dos.toUtf8().constData());
        int item_row = ui->listePaquets->row(item);
        gp.ajouterCarte(item_row, carte);
        std::cout << gp.paquetsDisponibles() << std::endl;
    }
}

//--------------------Fonction-------------------------//

void MainWindow::rafraichirAffichagePaquets(){
    ui->listePaquets->clear();
    int nbPaquet = gp.getPaquets().size();

    if(nbPaquet == 0){
        ui->listePaquets->addItem(NO_PAQUET);
        return;
    }

    int i;
    std::vector<PaquetCartes> paquets = gp.getPaquets();
    for(i = 0; i < nbPaquet; i++ ){
        QString nomItem = QString::fromStdString(paquets[i].getNom());
        ui->listePaquets->addItem( nomItem);
    }
}


