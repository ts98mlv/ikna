#include <iostream>
#include "Revision.hpp"
#include "GestionnairePaquet.hpp"
#include "PaquetCartes.hpp"
#include "Carte.hpp"

int main(int argc, char const *argv[]) { //début du main

  GestionnairePaquet gestion; //crée un gestionanire de paquets nommé gestion

  PaquetCartes paquet("Mon paquet"), paquet2("Mon deuxieme paquet"); //crée 2 paquets de cartes nommés "Mon paquet" et "Mon deuxieme paquet"

 //crée 3 cartes ; la première contient la question "Hey" et la réponse "Fin",
 //la deuxième a pour question "Pourquoi" et en réponse "Pas"
 // et la troisième a "OUI" pour question et "NOON" pour réponse
  Carte c1("Hey","Fin"), c2("Pourquoi","Pas"), c3("OUI","NOON");

  //crée 3 autres cartes ;
  //la première contient la question "HOHO" et la réponse "HIHI,
  //la deuxième a pour question "Enfin" et en réponse "Enfin !"
  // et la troisième a "Yolande est" pour question et "pas si yol que ça" pour réponse
  Carte d1("HOHO","HIHI"), d2("Enfin","Enfin !"), d3("Capital de la France","Paris");

  paquet.ajouterCarte(c1); //ajoute la carte c1 au paquet nommé "Mon paquet"
  paquet.ajouterCarte(c2); //ajoute la carte c2 au paquet nommé "Mon paquet"
  paquet.ajouterCarte(c3); //ajoute la carte c3 au paquet nommé "Mon paquet"

  paquet2.ajouterCarte(d1); //ajoute la carte d1 au paquet nommé "Mon deuxieme paquet"
  paquet2.ajouterCarte(d2); //ajoute la carte d2 au paquet nommé "Mon deuxieme paquet"
  paquet2.ajouterCarte(d3); //ajoute la carte d3 au paquet nommé "Mon deuxieme paquet"

  //ajoute le paquet nommé "Mon paquet" au gestionnaire de paquets
  gestion.ajouterPaquet(paquet);
  //ajoute le paquet nommé "Mon deuxieme paquet" au gestionnaire de paquets
  gestion.ajouterPaquet(paquet2);

  //affiche la description textuelle des paquets et de leur contenu sur la console
  std::cout << gestion.paquetsDisponibles() << std::endl;

  Revision r(paquet); //lance la révision du paquet nommé "Mon paquet"

  return 0;
}//fin du main
