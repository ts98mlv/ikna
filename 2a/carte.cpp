#include "carte.h"

Carte::Carte(std::string laFace, std::string leDos)
{
    this->face = laFace;
    this->dos = leDos;
}

std::string Carte::getFace(){
    return this->face;
}

std::string Carte::getDos(){
    return this->dos;
}
