#include <gestionnairepaquet.h>
#include "mainwindow.h"
#include <QApplication>
#include <fenetreapp.h>


int main(int argc, char *argv[]){
    QApplication a(argc, argv);

    FenetreApp fenetre;
    fenetre.show();

    return a.exec();
}

