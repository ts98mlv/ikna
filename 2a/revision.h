#ifndef REVISION_H
#define REVISION_H

#include <iostream>
#include <string>
#include <vector>
#include <carte.h>
#include <paquetcartes.h>

class Revision
{
private:
    //attributs
    PaquetCartes paquetRevise;
    Carte carteActuelle;
    std::vector<Carte> listeCartes;

    //fonctions
    void selectionCartes();
    void lancerRevision(std::vector<Carte> liste);



public:
    Revision(PaquetCartes lePaquet);
    Carte carteSuivante();
    void devoilerDos();
};

#endif // REVISION_H
