#include "paquetcartes.h"


PaquetCartes::PaquetCartes(std::string leNom)
{
    this->nom = leNom;
    this->listeCartes = std::vector<Carte>();
}

std::string PaquetCartes::getNom(){
    return this->nom;
}

void PaquetCartes::ajouterCarte(Carte c){
    this->listeCartes.push_back(c);
}
