#include "gestionnairepaquet.h"

GestionnairePaquet::GestionnairePaquet()
{
    this->listePaquets = std::vector<PaquetCartes>();
}

std::vector<PaquetCartes> GestionnairePaquet::getPaquets(){
    return this->listePaquets;
}

void GestionnairePaquet::ajouterPaquet(PaquetCartes paquet){
    this->listePaquets.push_back(paquet);
}

void GestionnairePaquet::ajouterCarte(int id, Carte carte){
    this->listePaquets.operator[](id).ajouterCarte(carte);
}
