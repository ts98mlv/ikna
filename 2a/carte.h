#ifndef CARTE_H
#define CARTE_H

#include <iostream>
#include <string>

class Carte
{
private:
    std::string face;
    std::string dos;

public:
    Carte(std::string laFace, std::string leDos);
    std::string getFace();
    std::string getDos();

};

#endif // CARTE_H
