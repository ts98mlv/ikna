#ifndef GESTIONNAIREPAQUET_H
#define GESTIONNAIREPAQUET_H

#include <iostream>
#include <string>
#include <vector>
#include <paquetcartes.h>

class GestionnairePaquet
{
private:
    std::vector<PaquetCartes> listePaquets;

public:
    GestionnairePaquet();
    std::vector<PaquetCartes> getPaquets();
    void ajouterPaquet(PaquetCartes);
    void ajouterCarte(int id, Carte carte);
};

#endif // GESTIONNAIREPAQUET_H
