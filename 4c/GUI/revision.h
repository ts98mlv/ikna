#ifndef REVISION_H
#define REVISION_H

#include <QWidget>

namespace Ui {
class Revision;
}

class Revision : public QWidget
{
    Q_OBJECT

public:
    explicit Revision(QWidget *parent = 0);
    ~Revision();

private:
    Ui::Revision *ui;
};

#endif // REVISION_H
