#include "revision.h"
#include "ui_revision.h"

Revision::Revision(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Revision)
{
    ui->setupUi(this);
    QObject::connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(affichageProchaineCarte()));
}

Carte Revision::carteSuivante(){
    Carte c = listeCartes.back();
    listeCartes.pop_back();
    return c;
}

void Revision::devoilerDos(Carte &c){
    QString nom_dos = QString::fromStdString(c.getDos());
    ui->text_dos->setText(nom_dos);
}

void Revision::afficherFace(Carte &c){
    QString nom_face = QString::fromStdString(c.getFace());
    ui->text_face->setText(nom_face);
}

void Revision::rafraichirAffichage(){
    ui->text_dos->clear();
    ui->text_face->clear();
}

void Revision::lancerRevision(){
    affichageProchaineCarte();
}

void Revision::selectionCartes(){
    int i;
    for(i = 0 ; i < paquetRevise.taille() ; i++){
      listeCartes.push_back(paquetRevise.get(i));
    }
}

void Revision::affichageProchaineCarte(){
    if(!listeCartes.empty()){

        QTimer *timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(finTimer()));

        rafraichirAffichage();
        ui->pushButton->setEnabled(false);


        carteActuelle = carteSuivante();
        afficherFace(carteActuelle);

        timer->start(1000 * DELAY);
    } else {
        this->hide();
        delete this;
    }

}

void Revision::definirPaquet(PaquetCartes& pc){
    paquetRevise = pc;
}

void Revision::attenteClicBouttonSuivant(){
    while (!isClic) {
        usleep(100);
    }
    isClic = false;
}

void Revision::finTimer(){
    devoilerDos(carteActuelle);
    ui->pushButton->setEnabled(true);
}
Revision::~Revision()
{
    delete ui;
}
