#-------------------------------------------------
#
# Project created by QtCreator 2018-12-03T10:27:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GUI
TEMPLATE = app


SOURCES += main.cpp\
        gui.cpp \
    mainwindow.cpp \
    revision.cpp

HEADERS  += gui.h \
    mainwindow.h \
    ../model/Carte.hpp \
    ../model/GestionnairePaquet.hpp \
    ../model/PaquetCartes.hpp \
    revision.h

FORMS    += gui.ui \
    mainwindow.ui \
    revision.ui
