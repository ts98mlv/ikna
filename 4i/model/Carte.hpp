#ifndef CARTE
#define CARTE

#include <iostream>
#include <string>
/*!
  @class Carte
  @authors Julien CLAISSE, Thomas SERRES et Clément MERCIER
  @brief La classe Carte est un modèle de carte. C'est cette dernière qui permet
   à l'utilisateur de stocker une question sur la face et sa réponse sur le dos.
*/
class Carte {
  private:
    std::string face;
    std::string dos;
  protected:

  public:
    //Constructeur
    /*!
    @fn Carte(std::string laFace, std::string leDos)
    @brief Constructeur qui permet de créer une carte avec en paramètres la question et la réponse
    @param[in] laFace La face de la carte, là où est stockée la question
    @param[in] leDos Le dos de la carte, là où est stockée la réponse
    @see face
    @see dos
    */
    Carte(std::string laFace, std::string leDos){
      face = laFace;
      dos = leDos;
    }
    Carte(){}

    //Destructeur
    ~Carte(){}

    /*!
    @fn std::string getFace()
    @brief fonction qui retourne l'attribut face
    @return l'attribut face de type std::string et qui correspond à la réponse
    @see face
    */
    std::string getFace() {
      return face;
    }

    /*!
    @fn std::string getDos()
    @brief fonction qui retourne l'attribut dos
    @return l'attribut dos de type std::string et qui correspond à la question
    @see dos
    */
    std::string getDos() {
      return dos;
    }

    /*!
    @fn std::string decrireCarte()
    @brief fonction qui renvoie une chaîne de caractères qui décrit une carte
    (question et réponse)
    @return une chaîne de caractères décrivant la carte
    */
    std::string decrireCarte() {
      return "Face : "+getFace()+" - Dos : "+ getDos();
    }
};


#endif
