#ifndef TESTQT_H
#define TESTQT_H

#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QMessageBox>


class TestQt : public QWidget
{
    Q_OBJECT

    public:
    TestQt();

    public slots:
    void ouvrirDialogue();
    void quitterApp();

private:
    QPushButton *m_boutonDialogue;
    QPushButton *quitter;
};

#endif // TESTQT_H
