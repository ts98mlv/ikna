#include "testqt.h"

TestQt::TestQt() : QWidget()
{
    setFixedSize(600, 400);

    m_boutonDialogue = new QPushButton("Ouvrir la boîte de dialogue", this);

    m_boutonDialogue->move(this->height()/2, this->width()/2);
    m_boutonDialogue->setToolTip("ouvre une boite de dialogue");
    m_boutonDialogue->setCursor(Qt::WaitCursor);
    m_boutonDialogue->setIcon(QIcon(QCoreApplication::applicationDirPath() + "/home.png"));
    m_boutonDialogue->setFont(QFont("Comic Sans MS", 20));

    quitter = new QPushButton("quitter", this);
    quitter->move(60, 10);
    quitter->setToolTip("permet de quitter l'appli");
    quitter->setCursor(Qt::CrossCursor);
    quitter->setIcon(QIcon(QCoreApplication::applicationDirPath() + "/home.png"));


    QObject::connect(m_boutonDialogue, SIGNAL(clicked()), this, SLOT(ouvrirDialogue()));
    QObject::connect(quitter, SIGNAL(clicked()), this, SLOT(quitterApp()));

}

void TestQt::ouvrirDialogue()
{
    //ouverture de boite de dialogue
    int reponse = QMessageBox::question(this, "Ma fen d'info", "Je <strong>teste</strong> le <strong>C++</strong> ! Et vous ?", QMessageBox::Yes | QMessageBox::No);

    if(reponse == QMessageBox::Yes){
        QMessageBox::information(this, "oui", "C'est bien je suis pas le seul :) ");
    }
    else{
        QMessageBox::critical(this, "non", "Tu devrais peut - être :/ ");
    }
}

void TestQt::quitterApp(){
   this->close();
}
