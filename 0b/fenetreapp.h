#ifndef FENETREAPP_H
#define FENETREAPP_H

#include <QDockWidget>

namespace Ui {
class FenetreApp;
}

class FenetreApp : public QDockWidget
{
   Q_OBJECT

public:
   explicit FenetreApp(QWidget *parent = nullptr);
   ~FenetreApp();

private:
   Ui::FenetreApp *ui;
};

#endif // FENETREAPP_H
