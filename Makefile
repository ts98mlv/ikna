	##
	#  Makefile écrit par Julien C.
	##

QT_REP = 0a 0b 1a 2a
CONSOLE_REP = 3a
NOM_GUI_REP = GUI
GUI_REP = 4a 4b 4c 4d 4e 4f 4g 4h 4i 4j
ALL_REP = $(QT_REP) $(CONSOLE_REP)

for_rep = for rep in $(1); do cd $$rep ; $(2); cd ..; done
for_rep_gui = for rep in $(1); do cd $$rep/$(NOM_GUI_REP) ; $(2); cd ../.. ; done

build: qmake
	$(call for_rep, $(ALL_REP), make)
	$(call for_rep_gui, $(GUI_REP), make)
	@$(call for_rep_gui, $(GUI_REP), mv GUI ../Ikna)

qmake :
	$(call for_rep, $(QT_REP), $@)
	$(call for_rep_gui, $(GUI_REP),$@)

clean:
	$(call for_rep, $(ALL_REP), make $@)
	$(call for_rep_gui, $(GUI_REP), make $@)
	@$(call for_rep, $(QT_REP), rm .qmake.stash)
	@$(call for_rep_gui, $(GUI_REP), rm .qmake.stash)

docs:
	$(call for_rep, $(CONSOLE_REP) $(GUI_REP), doxygen doxy.conf)

clean_makefile:
	@$(call for_rep, $(QT_REP), rm Makefile)
	@$(call for_rep_gui, $(GUI_REP), rm Makefile)

clean_docs:
	@$(call for_rep, $(CONSOLE_REP) $(GUI_REP), rm -rf docs)

clean_exe:
	@$(call for_rep, $(QT_REP) $(GUI_REP), rm -rf Ikna)
	@rm -f 0a/untitled

extra_clean: clean_exe clean_docs clean clean_makefile

zip: extra_clean
	@rm Ikna_Claisse_Mercier_Serres.zip
	$@ -r Ikna_Claisse_Mercier_Serres.zip $(ALL_REP) $(GUI_REP) CHANGELOG Makefile
