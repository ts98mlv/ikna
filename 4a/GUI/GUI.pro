#-------------------------------------------------
#
# Project created by QtCreator 2018-12-03T10:27:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GUI
TEMPLATE = app


SOURCES += main.cpp\
        gui.cpp

HEADERS  += gui.h

FORMS    += gui.ui
