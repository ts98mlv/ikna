#ifndef GESTIONNAIRE
#define GESTIONNAIRE

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "PaquetCartes.hpp"

/*!
  @class Carte
  @authors Julien CLAISSE, Thomas SERRES et Clément MERCIER
  @brief La classe GestionnairePaquet est une classe qui sert à gérer tous les
  paquets. C'est ainsi ici que l'on va ajouter un PaquetCartes, y ajouter des
   cartes, ...
*/
class GestionnairePaquet {
  private:
    std::vector<PaquetCartes> listePaquets;

  public:
    /*!
    @fn GestionnairePaquet()
    @brief construit un GestionnairePaquet
    */
    GestionnairePaquet(){}

    /*!
    @fn ajouterPaquet(PaquetCartes &paquet)
    @brief ajoute un PaquetCartes au gestionnaire de paquets
    @param[in] paquet Paquet à ajouter au gestionnaire
    */
    void ajouterPaquet(PaquetCartes &paquet){
      listePaquets.push_back(paquet);
    }

    /*!
    @fn ajouterCarte(int id, Carte &carte)
    @brief ajoute une Carte carte au PaquetCartes numéro id
    @param[in] id Numéro du paquet de cartes où on veut ajouter la carte
    @param[in] carte Carte à ajouter
    */
    void ajouterCarte(int id, Carte &carte){
      listePaquets[id].ajouterCarte(carte);
    }

    /*!
    @fn std::vector<PaquetCartes> getPaquets()
    @brief fonction qui retourne l'attribut listePaquets
    @return listePaquets attribut qui liste les paquets présents
    */
    std::vector<PaquetCartes> getPaquets(){
      return listePaquets;
    }

    /*!
    @fn std::string paquetsDisponibles()
    @brief fonction qui permet d'afficher textuellement les paquets de cartes et
    ce qu'ils contiennent
    @return res Chaîne qui contient la description textuelle des paquets et de
    leur contenu
    */
    std::string paquetsDisponibles(){
      std::string res = "";
      unsigned int i;
      for(i = 0; i < listePaquets.size(); i++){
        std::ostringstream os;
        os << i+1;
        res += "Paquet "+os.str()+". "+listePaquets[i].cartesDisponibles()+"\n";
        os.clear();
      }
      return res;
    }
};
#endif
