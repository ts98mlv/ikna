#ifndef  PAQUET
#define PAQUET

#include <iostream>
#include <string>
#include <vector>
#include "Carte.hpp"

/*!
    @brief La classe paquet de carte est un ensemble de carte
    C'est avec cette classe que l'utilisateur pourra effectuer des révisions
    @authors Mercier C., Claisse J., Serres T.
  */

class PaquetCartes{
  private:
    /// @brief Le nom du paquet de carte est une chaine de caractères
    std::string nom;

    /// @brief Ensemble des cartes dont contient le paquet
    std::vector<Carte> listeCartes;
  public:
    PaquetCartes() {}

    /*!
      @brief constructeur pricipal de la classe PaquetCartes
      Elle peux être accessible par toutes classes
      @param[in] leNom nom du paquet de carte attribué
      @see    nom
    */
    PaquetCartes(std::string leNom){
      nom = leNom;
    }

    /*!
      @brief Getteur de l'attribut nom
      @return attribut nom privée
      @see nom
    */
    std::string getNom(){
      return nom;
    }

    /*!
      @brief  ajoute une carte dans le paquet
      @param[in] c Carte à ajouter
      @see Carte
    */
    void ajouterCarte(Carte &c){
      listeCartes.push_back(c);
    }

    /*!
      @brief Obtenir une carte du paquet
      @param[in]  i indice de placement de la carte dans le paquet
      @return Carte au rang i du paquet
      @see Carte, listeCartes
    */
    Carte get(int i){
      return listeCartes[i];
    }

    /*!
      @brief obtenir la taille du paquet de carte
      @return la taille de la pile
      @see listeCartes
    */
    int taille() {
      return listeCartes.size();
    }

    /*!
      @brief décrit de manière textuels l'état actuel du paquet
      @return Message en chaine de caractere
      @see listeCartes
    */
    std::string cartesDisponibles() {
      std::string res = "Paquet "+getNom()+" : \n";
      unsigned int i;
      for(i = 0; i < listeCartes.size(); i++){
        res += "\t"+listeCartes[i].decrireCarte()+"\n";
      }
      return res;
    }
};

#endif
