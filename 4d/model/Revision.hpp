#ifndef REVISION
#define REVISION

#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
#include "PaquetCartes.hpp"
#include "Carte.hpp"

/*!
    @brief La classe Revision est un stade où l'utilisateur revise des cartes
    C'est cette classe qui gère la distribution des cartes à l'utilisateur selon la notation attribué
    @authors Mercier C., Claisse J., Serres T.
  */

class Revision{
  private:
    /// @brief Paquet de carte a réviser choisi par l'utilisateur
    /// @see PaquetCartes
    PaquetCartes paquetRevise;
    /// @brief Carte actuellement affiché sur l'écran
    /// @see PaquetCartes
    Carte carteActuelle;
    /// @brief liste des cartes selectionné par la classe revision
    /// @see selectionCartes()
    std::vector<Carte> listeCartes;

  public:
    /// @brief constructeur principal de la classe
    /// Elle peux être accessible par toutes classes
    /// @param[in] lePaquet paquet de carte à revisé choisi par l'utilisateur
    /// @see PaquetCartes
    Revision(PaquetCartes lePaquet){
      paquetRevise = lePaquet;
      selectionCartes();
      lancerRevision(listeCartes);
    }

    /// @brief permet d'obtenir la carte suivante
    /// @return carte suivante
    /// @see Carte, listeCartes
    Carte carteSuivante() {
      Carte c = listeCartes.back();
      listeCartes.pop_back();
      return c;
    }

    /// @brief permet d'afficher le dos de la carte de manière textuel
    /// @param[in] c Carte a devoiler
    /// @see Carte
    void devoilerDos(Carte& c) {
      std::cout << "Dos : " << c.getDos() << std::endl;
    }

  private:
    /// @brief selectionne les cartesà faire evaluer par l'utilisateur
    /// @see listeCartes, paquetRevise
    void selectionCartes() {
      int i;
      for(i = 0 ; i < paquetRevise.taille() ; i++){
        listeCartes.push_back(paquetRevise.get(i));
      }
    }

    /// @brief fonction principale permettant de reviser les cartes
    /// Les cartes sont tirées une à une, un temps de pause de reflexion est effectuée, puis l'utilisateur donne sa note
    /// Cette fonction affiche textuellement l'état de la revision
    /// @see listeCartes, Carte
    void lancerRevision(std::vector<Carte>& liste){
      while( !listeCartes.empty() ){
        Carte c = carteSuivante();
        std::cout << "Face : " << c.getFace() << '\n';
        usleep(2000 * 1000);
        devoilerDos(c);
        std::cout << "Presser ENTREE pour la carte suivante ... " << '\n';
        std::cin.ignore();
      }
      std::cout << "Fin test" << '\n';
    }
};

#endif
