#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   QObject::connect(ui->add_deck, SIGNAL(clicked()), this, SLOT(slot_ajouterpaquet()));
   this->rafraichirAffichagePaquets();
}

void MainWindow::slot_ajouterpaquet(){
    bool ok = false;
    QString nom_paquet = QInputDialog::getText(this, "Ajouter paquet", "Quel est le nom de votre nouveau paquet", QLineEdit::Normal, QString(), &ok);

    while(ok && nom_paquet.isEmpty()){
        QMessageBox::critical(this, "Pseudo", "Vous n'avez pas voulu donner votre nom… snif.");
        nom_paquet = QInputDialog::getText(this, "Ajouter paquet", "Quel est le nom de votre nouveau paquet", QLineEdit::Normal, QString(), &ok);
    }

    if(ok){
        PaquetCartes paquet(nom_paquet.toUtf8().constData());
        gp.ajouterPaquet(paquet);
        this->rafraichirAffichagePaquets();
    }
}

void MainWindow::rafraichirAffichagePaquets(){
    ui->listePaquets->clear();
    int nbPaquet = gp.getPaquets().size();

    if(nbPaquet == 0){
        ui->listePaquets->addItem("Aucuns paquets disponible ...");
        return;
    }

    int i;
    std::vector<PaquetCartes> paquets = gp.getPaquets();
    for(i = 0; i < nbPaquet; i++ ){
        QString nomItem = QString::fromStdString(paquets[i].getNom());
        ui->listePaquets->addItem( nomItem);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
