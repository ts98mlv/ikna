#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QInputDialog>
#include <QMessageBox>

#include "../model/GestionnairePaquet.hpp"


namespace Ui {
class MainWindow;
}

/*!
  @class MainWindow
  @authors Julien CLAISSE, Thomas SERRES et Clément MERCIER
  @brief La classe mainWindow peremet l'affichage de la fenêtre principale de ikna
*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
    @fn MainWindow(QWidget *parent = nullptr);
    @brief Constructeur qui permet de créer une fenetre principale de l'application
    @param[in] parent
    @see face
    @see dos
    */
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    /*!
    @fn slot_ajouterpaquet()
    @brief Slot appelé par un signal afin d'ajouter un paquet dans le gestionnaire
    @see GestionnairePaquet
    */
    void slot_ajouterpaquet();

private:
    Ui::MainWindow *ui;
    GestionnairePaquet gp;
    /*!
    @fn rafraichirAffichagePaquets()
    @brief Permet de rafraichir l'affichage des paquets
    @see GestionnairePaquet
    */
    void rafraichirAffichagePaquets();
};

#endif // MAINWINDOW_H
