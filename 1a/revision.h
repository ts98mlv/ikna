#ifndef REVISION_H
#define REVISION_H

#include <iostream>
#include <string>
#include <list>
#include <carte.h>
#include <paquetcartes.h>

class Revision
{
private:
    //attributs
    PaquetCartes paquetRevise;
    Carte carteActuelle;
    std::list<Carte> listeCartes;

    //fonctions
    void selectionCartes();
    void lancerRevision(std::list<Carte> liste);



public:
    Revision(PaquetCartes lePaquet);
    Carte carteSuivante();
    void devoilerDos();
};

#endif // REVISION_H
