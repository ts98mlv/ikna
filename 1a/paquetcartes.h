#ifndef PAQUETCARTES_H
#define PAQUETCARTES_H

#include <iostream>
#include <string>
#include <list>
#include <carte.h>

class PaquetCartes
{
private:
    std::string nom;
    std::list<Carte> listeCartes;

public:
    PaquetCartes(std::string leNom);
    std::string getNom();
    void ajouterCarte(Carte c);
};

#endif // PAQUETCARTES_H
