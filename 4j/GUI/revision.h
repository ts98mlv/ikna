#ifndef REVISION_H
#define REVISION_H
#define DELAY 10

#include <QDialog>
#include <QWidget>
#include <QTimer>
#include "../model/Carte.hpp"
#include "../model/PaquetCartes.hpp"
#include <vector>
#include <unistd.h>

namespace Ui {
class Revision;
}

class Revision : public QDialog
{
    Q_OBJECT

public:
    explicit Revision(QWidget *parent = 0);
    ~Revision();

private:
    Ui::Revision *ui;
  /// @brief Paquet de carte a réviser choisi par l'utilisateur
  /// @see PaquetCartes
  PaquetCartes paquetRevise;
  /// @brief Carte actuellement affiché sur l'écran
  /// @see PaquetCartes
  Carte carteActuelle;
  /// @brief liste des cartes selectionné par la classe revision
  /// @see selectionCartes()
  std::vector<Carte> listeCartes;
  /// @brief booleen qui permet de savoir si le bouton à été cliqué ou pas
  /// @see attenteClicBouttonSuivant
  bool isClic = false;

public:
  /// @brief definit le paquet à revisier
  /// @see PaquetCartes
  void definirPaquet(PaquetCartes& pc);

  /// @brief permet d'obtenir la carte suivante
  /// @return carte suivante
  /// @see Carte, listeCartes
  Carte carteSuivante();

  /// @brief permet d'afficher le dos de la carte
  /// @param[in] c Carte a devoiler
  /// @see Carte
  void devoilerDos(Carte& c);

  /// @brief permet d'afficher la face de la carte
  /// @param[in] c Carte a afficher
  /// @see Carte
  void afficherFace(Carte &c);

  /// @brief selectionne les cartesà faire evaluer par l'utilisateur
  /// @see listeCartes, paquetRevise
  void selectionCartes();

  /// @brief fonction principale permettant de reviser les cartes
  /// Les cartes sont tirées une à une, un temps de pause de reflexion est effectuée, puis l'utilisateur donne sa note
  /// Cette fonction affiche textuellement l'état de la revision
  /// @see listeCartes, Carte
  void lancerRevision();

  /// @brief Supprime les textes present dans les champs de texte face et dos
  void rafraichirAffichage();

  /// @brief Fonction bloquante en attendant le clic sur le boutton de l'utilisateur
  void attenteClicBouttonSuivant();

private:



public slots:


  /// @brief fonction executer a la fin du timer
  /// Est appeler apres la fin du délais d'attente de l'affichage de la face de la carte
  /// @see lancerRevision()
  void finTimer();

  /// @brief fonction executer au clic du boutton carter suivante de la revision
  /// Elle est aussi appelé en debut de revision pour afficher la premier carte
  /// @see lancerRevision() finTimer
  void affichageProchaineCarte();
};

#endif // REVISION_H
