#ifndef GESTIONNAIRE

#define GESTIONNAIRE
#define FICHIER_SAUVEGARDE "data.txt"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "PaquetCartes.hpp"



/*!
  @class Carte
  @authors Julien CLAISSE, Thomas SERRES et Clément MERCIER
  @brief La classe GestionnairePaquet est une classe qui sert à gérer tous les
  paquets. C'est ainsi ici que l'on va ajouter un PaquetCartes, y ajouter des
   cartes, ...
*/
class GestionnairePaquet {
  private:
    std::vector<PaquetCartes> listePaquets;

  public:
    /*!
    @fn GestionnairePaquet()
    @brief construit un GestionnairePaquet
    */
    GestionnairePaquet(){
       importerLesPaquets();
    }

    /*!
    @fn ajouterPaquet(PaquetCartes &paquet)
    @brief ajoute un PaquetCartes au gestionnaire de paquets
    @param[in] paquet Paquet à ajouter au gestionnaire
    */
    void ajouterPaquet(PaquetCartes &paquet){
      listePaquets.push_back(paquet);
       sauvegarderLesPaquets();
    }

    /*!
    @fn supprimerPaquet(PaquetCartes &paquet)
    @brief supprime un PaquetCartes au gestionnaire de paquets
    @param[in] indice du paquet Paquet à supprimer du gestionnaire
    */
    void supprimerPaquet(int indice_paquet){
        listePaquets.erase(listePaquets.begin() + indice_paquet);
         sauvegarderLesPaquets();
    }

    /*!
    @fn ajouterCarte(int id, Carte &carte)
    @brief ajoute une Carte carte au PaquetCartes numéro id
    @param[in] id Numéro du paquet de cartes où on veut ajouter la carte
    @param[in] carte Carte à ajouter
    */
    void ajouterCarte(int id, Carte &carte){
      listePaquets[id].ajouterCarte(carte);
      sauvegarderLesPaquets();
    }

    /*!
    @fn std::vector<PaquetCartes> getPaquets()
    @brief fonction qui retourne l'attribut listePaquets
    @return listePaquets attribut qui liste les paquets présents
    */
    std::vector<PaquetCartes> getPaquets(){
      return listePaquets;
    }

    /*!
    @fn void sauvegarderLesPaquets()
    @brief fonction qui sauvegarde l'état courant des paquets de cartes
    Le fichier se nomme data.txt
    */
    void sauvegarderLesPaquets(){
        //Ouverture du fichier en mode ecriture
        std::ofstream fichier(FICHIER_SAUVEGARDE, std::ios::out | std::ios::trunc);
        if(fichier){
            unsigned int i,j;
            //Ecrire pour chaque paquet
            for(i = 0; i < listePaquets.size(); i++){
                fichier << listePaquets[i].getNom() << "_";
                //Les cartes qu'elle contient
                for(j = 0 ; j < listePaquets[i].taille(); j++){
                    if(j != 0){
                        fichier << ",";
                    }
                    fichier << listePaquets[i].get(j).getFace() << "|" << listePaquets[i].get(j).getDos() ;
                }
                fichier << std::endl;
            }
            fichier.close();
        }else{
            std::cerr << "Impossible d'ouvrir le fichier" << std::endl;
        }
    }

    /*!
    @fn std::string paquetsDisponibles()
    @brief fonction qui permet d'afficher textuellement les paquets de cartes et
    ce qu'ils contiennent
    @return res Chaîne qui contient la description textuelle des paquets et de
    leur contenu
    */
    std::string paquetsDisponibles(){
      std::string res = "";
      unsigned int i;
      for(i = 0; i < listePaquets.size(); i++){
        std::ostringstream os;
        os << i+1;
        res += "Paquet "+os.str()+". "+listePaquets[i].cartesDisponibles()+"\n";
        os.clear();
      }
      return res;
    }

private:
    /*!
    @fn void importerLesPaquets()
    @brief fonction qui importe les paquets de cartes à partir des données enregistré
    Le fichier se nomme data.txt
    */
    void importerLesPaquets(){
        //Ouverture du fichier en mode lecture
        std::ifstream fichier(FICHIER_SAUVEGARDE, std::ios::in);
        if(fichier){
            unsigned int i;
            std::string ligne, nom_paquet, face, dos;
            std::vector<std::string> res, cartes, cotes;
            while(getline(fichier,ligne)){
                res = explode(ligne,'_');
                nom_paquet = res[0];
                PaquetCartes p(nom_paquet);
                cartes = explode(res[1],',');
                for(i = 0; i < cartes.size(); i++){
                    cotes = explode(cartes[i],'|');
                    face = cotes[0];
                    dos = cotes[1];
                    Carte c(face,dos);
                    p.ajouterCarte(c);
                }
                this->ajouterPaquet(p);
            }
            fichier.close();
        }
    }

    /*!
    @fn void explode(s,c)
    @brief fonction qui explose une chaine de caracteur en un ensemble de chaine selon un delimiteur
    @param[in] s String a exploser
    @param[in] c delimiteur
    @return vecteur de string
    */
    const std::vector<std::string> explode(const std::string& s, const char& c)
    {
        std::string buff{""};
        std::vector<std::string> v;

        for(auto n:s)
        {
            if(n != c) buff+=n; else
            if(n == c && buff != "") { v.push_back(buff); buff = ""; }
        }
        if(buff != "") v.push_back(buff);

        return v;
    }

};
#endif
